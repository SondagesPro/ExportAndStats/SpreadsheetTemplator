<?php
/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020-2022 Denis Chenu <http://www.sondages.pro>
 * @copyright 2020-2021 OECD (Organisation for Economic Co-operation and Development ) <www.oecd.org>
 * @license GPL
 * @version 1.4.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class SpreadsheetTemplator extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Use an excel file for export template.';
    protected static $name = 'SpreadsheetTemplator';

    /* private boolean isDone */
    private $isDone = false;

    public function init()
    {
        /* Add settings update */
        $this->subscribe('beforeToolsMenuRender');

        /* Dowload file */
        $this->subscribe('newDirectRequest');

        /* register alias */
        $this->subscribe('afterPluginLoad');
    }

    /** string[] allowed extension */
    private $allowedExtension = array("xlsx", "xlsm", "xls", "odt");

    /**
     * see beforeToolsMenuRender event
     * @deprecated ? See https://bugs.limesurvey.org/view.php?id=15476
     * @return void
     */
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aMenuItem = array(
            'label' => $this->gT('Spreadsheet Templator'),
            'iconClass' => 'fa fa-file-excel-o',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        $event->append('menuItems', array($menuItem));
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save'.get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }

        /* Basic setting */
        $templateToBeUsed = App()->getRequest()->getPost('templateToBeUsed');
        $this->set('templateToBeUsed', $templateToBeUsed, 'Survey', $surveyId);
        $sheetsToBeUpdated = App()->getRequest()->getPost('sheetsToBeUpdated');
        $this->set('sheetsToBeUpdated', $sheetsToBeUpdated, 'Survey', $surveyId);
        $filterQuestion = App()->getRequest()->getPost('filterQuestion');
        $this->set('filterQuestion', $filterQuestion, 'Survey', $surveyId);
        $filterValue = App()->getRequest()->getPost('filterValue');
        $this->set('filterValue', $filterValue, 'Survey', $surveyId);
        $filterValueByUrl = App()->getRequest()->getPost('filterValueByUrl');
        $this->set('filterValueByUrl', $filterValueByUrl, 'Survey', $surveyId);
        /* exports */
        $exports = App()->getRequest()->getPost('exports',array());
        foreach($exports as $extraSurveyId => $export) {
            /* Check export code */
            if(!empty($export['code'])) {
                $code = strval($export['code']);
                $fixedCode = preg_replace('/[^\da-z]/i', '', $code);
                if($fixedCode != $code) {
                    App()->setFlashMessage(sprintf($this->translate("Code \"%s\" update to \"%s\""),CHtml::encode($code),$fixedCode),'warning');
                }
                $exports[$extraSurveyId]['code'] = $fixedCode;
            }
        }
        $this->set('exports', $exports, 'Survey', $surveyId);

        if (App()->getRequest()->getPost('save'.get_class($this)) == 'redirect') {
            $redirectUrl = Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId));
            if (intval(App()->getConfig('versionnumber')) < 4) {
                $redirectUrl = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
            }
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        if (!Yii::getPathOfAlias('ExportResponseWithRelated')) {
            return "<div class='alert alert-danger'>" . $this->translate("You need ExportResponseWithRelated plugin") . "</div>";
        }
        $language = $oSurvey->language;
        $aData['pluginClass'] = get_class($this);
        $aData['surveyId'] = $surveyId;
        $aData['lang'] = array(
            'File to be used' => gT("File to be used"),
        );
        $aSettings=array();
        if(!Yii::getPathOfAlias('getQuestionInformation')) {
            $error = sprintf($this->translate("You need %sgetQuestionInformation%s plugin to export the data."),"<a href='https://gitlab.com/SondagesPro/coreAndTools/getQuestionInformation' rel='external'>","</a>");
            $aSettings[$this->translate('Unable to use this plugin')] = array(
                'link'=>array(
                    'type'=>'info',
                    'content'=> "<div class='alert alert-danger h4'>".$error."</div>"
                ,
                ),
            );
        }
        $templateToBeUsed = $this->get('templateToBeUsed', 'Survey', $surveyId, null);
        /* Excel file */
        $aFileToBeUsed = array(
            'templateToBeUsed' => array(
                'type'=>'select',
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                ),
                'label'=>$this->gT("File to be used"),
                'options'=> $this->getAvailableTemplates($surveyId),
                'current'=>$this->get('templateToBeUsed', 'Survey', $surveyId, null),
                'help' => sprintf($this->translate("You can put file in survey ressources. Plugin find files on upload/themes/survey/generalfiles too. This survey variables are Expression variables wrapped by Curly brackets : %s."),"<code>{QuestionTitle}</code>"),
            ),
            'sheetsToBeUpdated' => array(
                'type'=>'string',
                'label'=>$this->gT("Sheets to be updated"),
                'current'=>$this->get('sheetsToBeUpdated', 'Survey', $surveyId, 'data'),
                'help' => $this->translate("Only sheet start by this string are checked to be updated, set to empty to check all sheets.")."<br>"
                        . "<strong class='text-danger'>".$this->translate("Sheet with a lot of lines or columns can easily break the export.")."</strong>",
            ),
        );
        $aSettings[$this->translate("Template for this survey")] = $aFileToBeUsed;
        /* Survey part */
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId,App()->getLanguage());
        $surveyColumnsInformation->ByEmCode = true;
        $aFiltersQuestions = $surveyColumnsInformation->allQuestionListData();
        $aThisSurvey = array(
            'filterQuestion' => array(
                'type' => 'select',
                'htmlOptions'=>array(
                    'empty'=>$this->gT("None"),
                ),
                'label'=>$this->translate("Filter question"),
                'options'=> $aFiltersQuestions['data'],
                'current' => $this->get('filterQuestion', 'Survey', $surveyId, null),
            ),
            'filterValue' => array(
                'type' => 'string',
                'label' => $this->translate("Filter value"),
                'current' => $this->get('filterValue', 'Survey', $surveyId, ''),
            ),
            'filterValueByUrl' => array(
                'type'=>'select',
                'label'=>$this->translate("Can replace filter value in URL"),
                'options'=>array(
                    1 =>gT("Yes"),
                    0 =>gT("No"),
                ),
                'current'=>$this->get('filterValueByUrl', 'Survey', $surveyId, 0),
                'help' => $this->translate("User can set another value using <code>filter</code> parameters in url."),
            ),
        );
        $aSettings[$this->translate("More settings on data")] = $aThisSurvey;

        /* Export survey by type with code */
        $aSurveyEportType = array();
        $oAvailableRelatedExport = new \ExportResponseWithRelated\AvailableRelatedExport($surveyId);
        $aChildrensSurveyExport = $oAvailableRelatedExport->getChildrenSurveyExports();
        if(!empty($aChildrensSurveyExport)) {
            $aSurveyEportType['alertSurveyExport'] = array(
                'type' => 'info',
                'class' => 'well well-sm',
                'content' => "<p>".$this->translate("Surveys available for export with export type.")."<br>"
                            ."<strong class='text-warning'>".$this->translate("Survey id is saved as id, must review if you update related surveys.")."</strong></p>"
                            ."",
            );
            $aCurrentExports = $this->get('exports', 'Survey', $surveyId,array());
            foreach($aChildrensSurveyExport as $extraSurveyId => $aSurveyExport) {
                $sExportCode = !empty($aCurrentExports[$extraSurveyId]['code']) ? $aCurrentExports[$extraSurveyId]['code'] : "";
                $content = "<legend class='h5'>".Survey::model()->findByPk($extraSurveyId)->getLocalizedTitle()."</legend>"
                        .  "<div class='form-horizontal'>";
                /* the input text */
                $content.= "<div class='form-group'>";
                $content.= CHtml::label(
                    $this->translate("Export prefix"),
                    "exports_{$extraSurveyId}_code",
                    array("class"=>'col-sm-5 control-label')
                );
                $content.= "<div class='col-sm-7'>";
                $content.= CHtml::textField(
                    "exports[$extraSurveyId][code]",
                    $sExportCode,
                    array("class"=>'form-control','id'=>"exports_{$extraSurveyId}_code")
                );
                if(empty($sExportCode)) {
                    $content.= "<p class='help-block'>".$this->translate("Without prefix, no export is done")."</p>";
                } else {

                }
                $content.= "</div>";
                $content.= "</div>";
                /* the select */
                $aExportExports = !empty($aCurrentExports[$extraSurveyId]['export']) ? $aCurrentExports[$extraSurveyId]['export'] : array();
                $aExportExports = array_filter($aExportExports, function($code) use ($aSurveyExport) {
                    return array_key_exists($code, $aSurveyExport );
                });
                $content.= "<div class='form-group'>";
                $properties = array(
                    'data' => $aSurveyExport,
                    'name' => "exports[$extraSurveyId][export]",
                    'value' => $aExportExports,
                    'pluginOptions' => array(
                        'minimumResultsForSearch' => 8,
                        'dropdownAutoWidth'=> true,
                        'width' => "js: function(){ return Math.max.apply(null, $(this.element).find('option').map(function() { return $(this).text().length; }))+'em' }",
                    ),
                    'htmlOptions'=>array(
                        'multiple'=> true,
                    ),
                );
                $content.= CHtml::label(
                    $this->translate("Export type"),
                    "exports_{$extraSurveyId}_exports",
                    array("class"=>'col-sm-5 control-label')
                );
                $content.= "<div class='col-sm-7'>";
                $content.= App()->getController()->widget('yiiwheels.widgets.select2.WhSelect2', $properties, true);
                $content.= "</div>";
                $content.= "</div>";

                /* the add total option */
                $bExportTotal = !empty($aCurrentExports[$extraSurveyId]['total']) ? true : false;
                $content.= "<div class='form-group'>";
                $content.= CHtml::label(
                    $this->translate("Export total"),
                    "exports_{$extraSurveyId}_total",
                    array("class"=>'col-sm-5 control-label')
                );
                $content.= "<div class='col-sm-7'>";
                $content.= App()->getController()->widget('yiiwheels.widgets.switch.WhSwitch', array(
                    'name' => "exports[$extraSurveyId][total]",
                    'value' => $bExportTotal,
                    'onLabel'=>gT('On'),
                    'offLabel' => gT('Off'),
                    'htmlOptions' => array(
                        'id'=>"exports_{$extraSurveyId}_total",
                        'uncheckValue' => 0
                    ),
                ), true);
                $content.= "<p class='help-block'>".$this->translate("If you have filtered export : this allow to export the «total» line")."</p>";
                $content.= "</div>";
                $content.= "</div>";

                /* the force total to 0 option */
                $bExportTotal = !empty($aCurrentExports[$extraSurveyId]['totalforce']) ? true : false;
                $content.= "<div class='form-group'>";
                $content.= CHtml::label(
                    $this->translate("Force total line"),
                    "exports_{$extraSurveyId}_totalforce",
                    array("class"=>'col-sm-5 control-label')
                );
                $content.= "<div class='col-sm-7'>";
                $content.= App()->getController()->widget('yiiwheels.widgets.switch.WhSwitch', array(
                    'name' => "exports[$extraSurveyId][totalforce]",
                    'value' => $bExportTotal,
                    'onLabel'=>gT('On'),
                    'offLabel' => gT('Off'),
                    'htmlOptions' => array(
                        'id'=>"exports_{$extraSurveyId}_totalforce",
                        'uncheckValue' => 0
                    ),
                ), true);
                $content.= "<p class='help-block'>".$this->translate("If there are no data  this force to have a the «total» line (only if you export the «total» line)")."</p>";
                $content.= "</div>";
                $content.= "</div>";
                /* the NA */
                $sNAtext = !empty($aCurrentExports[$extraSurveyId]['NAtext']) ? true : false;
                $content.= "<div class='form-group'>";
                $content.= CHtml::label(
                    $this->translate("Not applicable string"),
                    "exports_{$extraSurveyId}_NAtext",
                    array("class"=>'col-sm-5 control-label')
                );
                $content.= "<div class='col-sm-7'>";
                $content.= CHtml::textField(
                    "exports[$extraSurveyId][NAtext]",
                    $sNAtext,
                    array("class"=>'form-control','id'=>"exports_{$extraSurveyId}_NAtext")
                );
                $content.= "<p class='help-block'>".$this->translate("String to use when evaluation is not applicable (Division by 0 for example")."</p>";
                $content.= "</div>";
                $content.= "</div>";

                /* The exports available detail */
                if(!empty($sExportCode) && !empty($aExportExports)) {
                    $content.= "<div class='well'><ul class='list-unstyled'>";
                    foreach($aExportExports as $exportcode) {
                        $code = 
                        $title = $aSurveyExport[$exportcode];
                        $content.= "<li>";
                        $content.= sprintf($this->translate("Use %s in your xls for %s."),"<code>[{$sExportCode}_{$exportcode}.QuestionTitle]</code>","<em>".$title."</em>");
                        $content.= "</li>";
                    }
                    $content.= "</ul>";
                    $content.= "<p>".sprintf($this->translate("%s must be replaced by variables from expression manager, can be question title or fixed data. See VV export for sample."),"<code>QuestionTitle</code>")."</p>";
                    $content.= "</div>";
                }
                $content.= "</div>";
                $aSurveyEportType['surveyExportInfo'.$extraSurveyId] = array(
                    'type' => 'info',
                    'content' => $content,
                );
                
            }
        }
        $aSettings[$this->translate("Related survey to be exported")] = $aSurveyEportType;

        /* Dowload links */
        /* Move it to view ? */
        $htmlLink  = "<div class='well'>";
        $htmlLink .= "<div class='h4'>".$this->translate("Links for download export file")."</div>";
        $htmlLink .= "<ul>";
        $htmlLink .= "<li>".Chtml::link(
            $this->translate("All submitted response (default)"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId)
        ).'</li>';
        $htmlLink .= "<li>".Chtml::link(
            $this->translate("Token response"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'state' => "{TOKEN}")
        ).'</li>';
        $htmlLink .= "<li>".Chtml::link(
            $this->translate("Response by id"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'srid' => "{SAVEDID}")
        ).'</li>';
        $htmlLink .= "<li>".Chtml::link(
            $this->translate("Only differences"),
            array('plugins/direct', 'plugin' => get_class(), 'function' => 'export', 'surveyid' => $surveyId, 'diff' => '1')
        ).'</li>';
        $htmlLink .= "</ul>";
        $htmlLink .= "</div>";
        $htmlCodeLink = "<div class='well'>";
        $htmlCodeLink .= "<div class='h4'>".$this->translate("URL to download differences file inside survey")."</div>";
        $htmlCodeLink .= "<ul>";
        $htmlCodeLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data with token"))
                  . Chtml::tag(
                        "code",
                        array(),
                        str_replace(
                            array('SID','TOKEN'),
                            array('{SID}','{TOKEN}'),
                            Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid'=>$surveyId,'token'=>'TOKEN'))
                        )
                  )
                  . "</li>";
        $htmlCodeLink .= "<li>"
                  . CHtml::tag('strong', array(), $this->translate("All data related to current response"))
                  . Chtml::tag(
                        "code",
                        array(),
                        str_replace(
                            array('SID','SAVEDID'),
                            array('{SID}','{SAVEDID}'),
                            Yii::app()->createUrl("plugins/direct", array('plugin' => get_class(),'surveyid'=>$surveyId,'srid'=>'SAVEDID'))
                        )
                  )
                  . "</li>";
        $htmlCodeLink .= "</ul>";
        $htmlCodeLink .= "</div>";
        $aExportLinks = array(
            'demoHtmlCodeLink' => array(
                'type' => 'info',
                'content' => $htmlCodeLink
            ),
        );
        $aSettings[$this->translate("Export links")] = $aExportLinks;

        $aData['aSettings']=$aSettings;
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyId' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyId' => $surveyId)),
            'close' => Yii::app()->createUrl('surveyAdministration/view', array('surveyid' => $surveyId)),
        );
        if (intval(App()->getConfig('versionnumber')) < 4) {
            $aData['form']['close'] = Yii::app()->createUrl('admin/survey/sa/view', array('surveyId' => $surveyId));
        }
        $content = $this->renderPartial('admin.settings', $aData, true);
        return $content;
    }


    public function newDirectRequest()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if ($this->event->get('target') != get_class()) {
            return;
        }
        $function = $this->event->get('function');
        $surveyid = App()->getRequest()->getParam('surveyid', App()->getRequest()->getParam('sid'));
        if (empty($surveyid)) {
            throw new CHttpException(400, $this->translate("No surveyid"));
        }
        if (empty(Survey::model()->findByPk($surveyid))) {
            throw new CHttpException(404, $this->translate("Invalid survey id"));
        }
        $token = App()->getRequest()->getParam('token');
        $srid = App()->getRequest()->getParam('srid');
        $language = App()->getRequest()->getParam('lang');
        $filter = App()->getRequest()->getParam('filter');
        switch ($function) {
            case 'export':
            default:
                $this->ExportTemplates($surveyid, $token, $srid, $language, $filter);
        }
    }

    /**
     * Export excel file by template
     * Leave it public : permission checked (or token set in url)
     * @param int $surveyId
     * @param string $token
     * @param integer $srid
     * @param string $language
     * @param string $filter
     * @return void
     */
    public function ExportTemplates($surveyId, $token = null, $srid = null, $language = null, $filter = null)
    {
        Yii::import('application.helpers.viewHelper');
        /* Check if $srid is OK */
        if ($srid && !Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export') && $srid) {
            $sessionSrid = isset($_SESSION['survey_'.$surveyId]['srid']) ? $_SESSION['survey_'.$surveyId]['srid'] : null;
            if ($sessionSrid != $srid) {
                throw new CHttpException(403, $this->translate("No rights on this survey."));
            }
        }
        $xlsFile = $this->getFinalFileName($surveyId, $this->get('templateToBeUsed', 'Survey', $surveyId,""));
        if(!$xlsFile) {
            throw new CHttpException(400, $this->translate("Invalid survey."));
        }
        if (empty($srid) && empty($token) && !Permission::getUserId()) {
            throw new CHttpException(401);
        }
        if (empty($srid) && empty($token) && !Permission::model()->hasSurveyPermission($surveyId, 'responses', 'export')) {
            throw new CHttpException(403, $this->translate("No rights on this survey."));
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!empty($token) && ($oSurvey->anonymized == 'Y' || !$oSurvey->hasTokensTable)) {
            throw new CHttpException(400, $this->translate("Invalid token parameter for this survey."));
        }
        if (empty($language) || !in_array($language, $oSurvey->getAllLanguages())) {
            $language = $oSurvey->language;
        }
        /* Get current response as EMcode=>value */
        if(empty($srid) && !empty($token)) {
            $tokenResponse = \SurveyDynamic::model($surveyId)->find("token = :token",array(":token" => $token ));
            if(empty($tokenResponse)) {
                throw new CHttpException(404, $this->translate("Invalid token."));
            }
            $filterQuestion = $this->get('filterQuestion', 'Survey', $surveyId,"");
            if ($filterQuestion) {
                $aCodeQuestions = array_flip(getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId));
                if (isset($aCodeQuestions[$filterQuestion])) {
                    $sColumn = $aCodeQuestions[$filterQuestion];
                    $filterValue = $this->get('filterValue', 'Survey', $surveyId,"");
                    if($this->get('filterValueByUrl', 'Survey', $surveyId,"")) {
                        $filterValue = App()->getRequest()->getParam('filter', $filterValue);
                    }
                    $criteria = new CDbCriteria;
                    $criteria->compare('token', $token);
                    $criteria->compare(\App()->getDb()->quoteColumnName($sColumn), $filterValue);
                    $tokenResponse = \SurveyDynamic::model($surveyId)->find($criteria);
                    if(empty($tokenResponse)) {
                        throw new CHttpException(404, $this->translate("Invalid filter."));
                    }
                } else {
                    // Todo : throw error for admin or log for user
                }
                
            }
            $srid = $tokenResponse->id;
        }

        $this->ExportTemplate($surveyId, $srid, $language);
        /* Stop it */
        Yii::app()->end();
    }

    /**
     * Export template for one srid
     * @param int $surveyId
     * @param integer $srid
     * @param string $language
     */
    private function ExportTemplate($surveyId, $srid = null, $language = null)
    {
        if(!Yii::getPathOfAlias('getQuestionInformation')) {
            throw new CHttpException(500, gT("Need getQuestionInformation plugin."));
        }
        Yii::setPathOfAlias("SpreadsheetTemplator", dirname(__FILE__));
        $xlsFile = $this->getFinalFileName($surveyId, $this->get('templateToBeUsed', 'Survey', $surveyId,""));
        if(!$xlsFile) {
            throw new CException("Invalid survey.");
        }
        $SpreadsheetExport = new \SpreadsheetTemplator\exports\SpreadsheetExportTbs($surveyId, $srid, $language);
        if (!empty($this->get('exports', 'Survey', $surveyId,array()))) {
            $SpreadsheetExport->aRelatedExports = $this->get('exports', 'Survey', $surveyId,array());
        }
        $SpreadsheetExport->sheetsForData = $this->get('sheetsToBeUpdated', 'Survey', $surveyId,'data');
        $SpreadsheetExport->export($xlsFile);
    }
    /**
     * Get the avaiable file for this surey
     * This function is not efficient so should only be used in the admin interface
     * that specifically deals with enabling / disabling plugins.
     */
    private function getAvailableTemplates($surveyId = false)
    {
        $result = array();
        $globalDir = App()->getConfig('userthemerootdir').DIRECTORY_SEPARATOR.'generalfiles';
        if (is_dir($globalDir)) {
            foreach (new \DirectoryIterator($globalDir) as $fileInfo) {
                if(in_array($fileInfo->getExtension(),$this->allowedExtension)) {
                    $result[$fileInfo->getFilename()] = $fileInfo->getFilename();
                }
            }
        }
        $surveyDir = App()->getConfig('uploaddir').DIRECTORY_SEPARATOR.'surveys'.DIRECTORY_SEPARATOR.$surveyId.DIRECTORY_SEPARATOR.'files';
        if (is_dir($surveyDir)) {
            foreach (new \DirectoryIterator($surveyDir) as $fileInfo) {
                if(in_array($fileInfo->getExtension(),$this->allowedExtension)) {
                    $result[$fileInfo->getFilename()] = $fileInfo->getFilename();
                }
            }
        }
        return $result;
    }

    /**
     * return final file
     * @param string : filename
     * @return string|false
     */
    private function getFinalFileName($surveyId, $fileName)
    {
        if(empty($fileName)) {
            return false;
        }
        if(!pathinfo($fileName,PATHINFO_EXTENSION) || !in_array(pathinfo($fileName,PATHINFO_EXTENSION),$this->allowedExtension) ) {
            return false;
        }
        $dirPath = realpath(App()->getConfig('uploaddir').DIRECTORY_SEPARATOR.'surveys'.DIRECTORY_SEPARATOR.$surveyId.DIRECTORY_SEPARATOR.'files');
        $finalFileName = realpath($dirPath.DIRECTORY_SEPARATOR.$fileName);
        if(is_file($finalFileName) && is_readable($finalFileName) && substr($finalFileName, 0, strlen($dirPath)) == $dirPath) {
            return $finalFileName;
        }
        $dirPath = realpath(App()->getConfig('userthemerootdir').DIRECTORY_SEPARATOR.'generalfiles');
        $finalFileName = realpath($dirPath.DIRECTORY_SEPARATOR.$fileName);
        if(is_file($finalFileName) && is_readable($finalFileName) && substr($finalFileName, 0, strlen($dirPath)) == $dirPath) {
            return $finalFileName;
        }
        return false;
    }
    /**
     * Fix the DB value
     * $value mixed
     * $type string
     * @return mixed
     */
    private function fixDbValue($value,$type)
    {
        if($type == 'number') {
            if ($value && $value == ".") {
                $value = "0" . $value;
            }
            if (strpos($value, ".")) {
                $value = rtrim(rtrim($value, "0"), ".");
            }
        }
        return $value;
    }

    /**
     * Process a string by EM
     * @param string $string
     * @return string
     */
    private function processString($string)
    {
        /* Commented : work for date even if not initilized */
        //~ if(!LimeExpressionManager::isInitialized()) {
            //~ return $string;
        //~ }
        return LimeExpressionManager::ProcessStepString($string,array(),3,true);
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable(array($this, 'gT'))) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }

    public function afterPluginLoad()
    {
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
    }
}

# SpreadsheetTemplator

Allow to use a Spreadsheet (xlsx) template to produce report.

This plugin need [ExportResponseWithRelated](https://gitlab.com/SondagesPro/ExportAndStats/ExportResponseWithRelated).

This plugin need PHP 7.2

## Simple usage

The plugin allow to construct an XLSX page with value replaced by current survey. The plugin use [TinyButStrong template engine](https://www.tinybutstrong.com/).

Variable that can be used :

- Current survey questions [survey.QuestionCode], QuestionCode can be any question using Expression Manager code.
    - You can force some cell parameter using [ope paramleters](https://www.tinybutstrong.com/opentbs.php?doc#cells), for example for a numeric question type : `[survey.age;ope=tbs:num]`
- Related survey with questionSpreadSheetSurvey or questionExtraSurvey can be used on the same way and are sent as array.
    - You must use `[block=tbs:row]`(https://www.tinybutstrong.com/opentbs.php?doc#blockalias) to create a row for each related response.

<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2020 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 1.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
namespace SpreadsheetTemplator\exports;

require_once(dirname(__FILE__).'/../vendor/autoload.php');
require_once(dirname(__FILE__).'/../vendor/tinybutstrong/tinybutstrong/tbs_class.php'); // Load the TinyButStrong template engine
require_once(dirname(__FILE__).'/../vendor/tinybutstrong/opentbs/tbs_plugin_opentbs.php'); // Load the OpenTBS plugin

use clsTinyButStrong;
use clsOpenTBS;

use Yii;
use CException;
use Survey;
use Response;

class SpreadsheetExportTbs
{
    /* var integer surveyid */
    public $surveyId = 0;
    /* var integer responseId */
    public $responseId = 0;

    /* var string|null language */
    public $language;

    /* var array[] */
    public $aRelatedExports = array();

    //~ /* var string|null xls file */
    //~ public $spreadsheetFile;

    /* var string Sheet to be checked for updating xls file */
    public $sheetsForData;

    /**
     * conctructor
     * @param Object Plugin extend PluginBase
     * @param integer surveyid
     * @param string language
    **/
    public function __construct($surveyId, $responseId, $language = null)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CException("Invalid surveyId");
        }
        if ($oSurvey->active != "Y") {
            throw new CException("Invalid survey");
        }
        $this->surveyId = $surveyId;
        $oResponse = Response::model($surveyId)->findByPk($responseId);
        if (!$oResponse) {
            throw new CException("Invalid responseId");
        }
        $this->responseId = $responseId;
        $aResponse = $oResponse->getAttributes();
        if (is_null($language) && !empty($oResponse->startlanguage) ) {
            $language = $oResponse->language;
        }
        if (is_null($language)) {
            $language = $oSurvey->language;
        } elseif (!in_array($language, $oSurvey->getAllLanguages())) {
            throw new CException("Invalid language");
        }
        $this->language = $language;
    }

    /**
     * Set the compile question
     * @param string the xls file to use
     * @throw error
     * @return array|void
     */
    public function export($spreadsheetFile)
    {
        $exportResponse = new \ExportResponseWithRelated\exports\Responses($this->surveyId,$this->language);
        $exportResponse->responseId = $this->responseId;
        $exportResponse->completed = false;
        foreach($this->aRelatedExports as $surveyId => $arelatedExport) {
            if(!empty($arelatedExport['code']) && !empty($arelatedExport['export'])) {
                $options = array();
                if (!empty($arelatedExport['total'])) {
                    $options['alltotal'] = true;
                    if (!empty($arelatedExport['totalforce'])) {
                        $options['totalforced'] = true;
                    }
                }
                if (isset($arelatedExport['NAtext'])) {
                    $options['NATextValue'] = $arelatedExport['NAtext'];
                }
                $exportResponse->addRelatedExport($surveyId, $arelatedExport['code'], $arelatedExport['export'],$options);
            }
        }
        $aExportDatas = $exportResponse->export();
        if(empty($aExportDatas)) {
            throw new CException("No data to be exported");
        }
        $aExportData = $aExportDatas[0];
        $aRelatedDatas = $exportResponse->exportRelated();
        /* Final file name */
        $dirName = dirname($spreadsheetFile);
        $fileName = basename($spreadsheetFile);
        $match = preg_match_all('#{(.*?)}#is', $fileName, $aMatcheds);
        if($match) {
            $aSearch = $aMatcheds[0];
            $aReplace = array();
            $aExportData = $aExportDatas[0];
            foreach ($aMatcheds[1] as $replace) {
                if(isset($aExportData[$replace])) {
                    $aReplace[] = $aExportData[$replace];
                } else {
                    $aReplace[] = $replace;
                }
            }
            $fileName = str_replace($aSearch,$aReplace,$fileName);
        }
        $sheetsNameToUpdate = $this->getSheetsNameToUpdate($spreadsheetFile);
        /* Construct the data */
        $aDataForXls['survey'][] = $aExportData;
        foreach($aRelatedDatas as $prefix => $aExports) {
            foreach($aExports as $export => $aIndexedValues) {
                $aDataForXls[$prefix."_".$export] = $aIndexedValues;
            }
        }
        $TBS = new clsTinyButStrong;
        $TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
        $TBS->LoadTemplate($spreadsheetFile, OPENTBS_ALREADY_UTF8);
        foreach ($sheetsNameToUpdate as $sheetName) {
            $TBS->PlugIn(OPENTBS_SELECT_SHEET, $sheetName);
            foreach($aDataForXls as $key => $data) {
                $TBS->MergeBlock($key, $data); 
            }
        }
        
        $TBS->Show(OPENTBS_DOWNLOAD, $fileName);
        App()->end();
    }

    /**
     * Get the list of sheets name to be updated
     * param string a valid $spreadsheetFile
     * return string[]
     */
    private function getSheetsNameToUpdate($spreadsheetFile)
    {
        $sheetsNameToUpdate = array();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($spreadsheetFile);
        $sheetsForData = trim($this->sheetsForData);
        $sheetsName = $spreadsheet->getSheetNames();
        foreach($sheetsName as $sheetName) {
            if(empty($sheetsForData) || substr($sheetName, 0, strlen($sheetsForData)) == $sheetsForData) {
                $sheetsNameToUpdate[] = $sheetName;
            }
        }
        unset($reader);
        unset($spreadsheet);
        return $sheetsNameToUpdate;
    }     
}
